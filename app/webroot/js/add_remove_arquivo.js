$(document).ready(function() {
	/** Add **/
	$('#adicionar_arquivo').click(function(e) {
		$.ajax({
			url: '/mensagens/ajax_add_arquivo',
			beforeSend: function() {
				$('#loader').show();
			},
			success: function(data) {
				$(data).hide().insertAfter('#adicionar_arquivo').fadeIn(1000);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				alert('Ocorreu um erro com a sua requisição. Por favor, recarregue a página e tente novamente.');
			},
			complete: function() {
				$('#loader').hide();
			}
		});
		
		e.preventDefault();
	});

	/** Remove **/
	$('#arquivos').on('click', '.remover_arquivo', function(e) {
		$(this).closest('.input-group').fadeOut(500, function() {
			$(this).remove();
		});
		
		e.preventDefault();
	});
});
