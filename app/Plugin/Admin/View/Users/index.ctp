<div class="row">
    <div class="col-xs-12">
        <h2>Usu&aacute;rios</h2>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <?php echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-plus')) . ' Adicionar', array('action' => 'add'), array('class' => 'btn btn-primary', 'escapeTitle' => false)); ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Nome de Usu&aacute;rio</th>
                        <th>A&ccedil;&otilde;es</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($users as $key => $user): ?>
                    <tr>
                        <td><?php echo $user['User']['username']; ?></td>
                        <td><?php echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-remove')), array('action' => 'delete', $user['User']['id']), array('class' => 'btn btn-default', 'title' => 'Deletar', 'escapeTitle' => false), 'Tem certeza que deseja deletar o usuário '.$user['User']['username'].'?'); ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php echo $this->element('layout/paginacao'); ?>
