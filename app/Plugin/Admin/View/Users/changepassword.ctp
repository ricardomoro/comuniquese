<div class="row">
    <div class="col-xs-12">
        <h2>Alterar Senha</h2>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
    <?php
        echo $this->Form->create('User');
            echo $this->Form->input('id');
    ?>
            <div class="form-group">
                <label class="col-sm-2 control-label">Usu&aacute;rio</label>
                <div class="col-sm-10">
                    <p class="form-control-static"><em><?php echo $this->Session->read('Auth.User.username'); ?></em></p>
                </div>
            </div>
    <?php
            echo $this->Form->input('current_password', array('type' => 'password', 'label' => 'Senha Atual'));
            echo $this->Form->input('new_password1', array('type' => 'password', 'label' => 'Nova Senha'));
            echo $this->Form->input('new_password2', array('type' => 'password', 'label' => 'Repita a Nova Senha'));
        echo $this->Form->end('Alterar');
    ?>
    </div>
</div>
