<div class="row">
    <div class="col-xs-12">
        <h2>Entrar no Sistema</h2>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
    <?php
        echo $this->Form->create('User');
            echo $this->Form->input('User.username', array('type' => 'text', 'label' => 'Usuário'));
            echo $this->Form->input('User.password', array('type' => 'password', 'label' => 'Senha'));
        echo $this->Form->end('Entrar');
    ?>
    </div>
</div>
