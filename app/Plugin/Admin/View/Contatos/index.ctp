<div class="row">
    <div class="col-xs-12">
        <h2>Contatos</h2>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <?php echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-plus')) . ' Adicionar', array('action' => 'adicionar'), array('class' => 'btn btn-primary', 'escapeTitle' => false)); ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Email</th>
                        <th>Nome</th>
                        <th>A&ccedil;&otilde;es</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    foreach ($contatos as $key => $contato):
                ?>
                        <tr>
                            <td><?php echo $contato['Contato']['email']; ?></td>
                            <td><?php echo $contato['Contato']['nome']; ?></td>
                            <td>
                                <?php echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-edit')), array('action' => 'editar', $contato['Contato']['id']), array('class' => 'btn btn-default', 'title' => 'Editar', 'escapeTitle' => false)); ?>
                                <?php echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-remove')), array('action' => 'deletar', $contato['Contato']['id']), array('class' => 'btn btn-default', 'title' => 'Deletar', 'escapeTitle' => false), 'Tem certeza que deseja deletar o email '.$contato['Contato']['email'].'?'); ?>
                            </td>
                <?php
                    endforeach;
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php echo $this->element('layout/paginacao'); ?>
