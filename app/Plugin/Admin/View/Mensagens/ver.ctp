<div class="row">
    <div class="col-xs-12">
        <h2>Visualizar Mensagem</h2>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <p><strong>De: </strong><?php echo $mensagem['Mensagem']['nome']; ?> &lt;<?php echo $mensagem['Mensagem']['email']; ?>&gt;<?php echo ($mensagem['Mensagem']['telefone']) ? '<small> | '.$mensagem['Mensagem']['telefone'].'</small>' : ''; ?></p>
                <p><strong>Para: </strong><?php echo $mensagem['Contato']['nome']; ?> &lt;<?php echo $mensagem['Contato']['email']; ?>&gt;</p>
            </div>
            <div class="panel-body">
                <p>
                    <strong>O qu&ecirc;?</strong>
                    <br/>
                    <?php echo $mensagem['Mensagem']['oque']; ?>
                </p>
                <p>
                    <strong>Quem?</strong>
                    <br/>
                    <?php echo $mensagem['Mensagem']['quem']; ?>
                </p>
                <p>
                    <strong>Quando?</strong>
                    <br/>
                    <?php echo $mensagem['Mensagem']['quando']; ?>
                </p>
                <p>
                    <strong>Onde?</strong>
                    <br/>
                    <?php echo $mensagem['Mensagem']['onde']; ?>
                </p>
                <p>
                    <strong>Como?</strong>
                    <br/>
                    <?php echo $mensagem['Mensagem']['como']; ?>
                </p>
                <p>
                    <strong>Por qu&ecirc;?</strong>
                    <br/>
                    <?php echo $mensagem['Mensagem']['porque']; ?>
                </p>
                <p>
                    <strong>Outras Informa&ccedil;&otilde;es</strong>
                    <br/>
                    <?php echo $mensagem['Mensagem']['outras_informacoes']; ?>
                </p>

                <?php if (!empty($mensagem['Anexo'])): ?>
                <h4>ANEXOS</h4>
                <div class="list-group">
                    <?php foreach ($mensagem['Anexo'] as $key => $anexo): ?>
                        <a target="_blank" class="list-group-item" href="/files/uploads/<?php echo $anexo['arquivo']; ?>"><?php echo $anexo['nome']; ?></a>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->element('layout/voltar_ao_index'); ?>
