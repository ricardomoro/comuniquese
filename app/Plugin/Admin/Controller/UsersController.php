<?php
class UsersController extends AdminAppController
{
    public $name = 'Users';
    public $uses = array('User');

    public $components = array('Paginator');
    public $paginate = array(
        'User' => array(
            'limit' => 10,
            'order' => array(
                'user.id' => 'asc',
            ),
        ),
    );


    public function isAuthorized($user) {
        if (in_array($this->action, array('index', 'add', 'delete'))) {
            if ($user['role'] != 'root') {
                return false;
            }
        }
        return parent::isAuthorized($user);
    }


    public function index()
    {
        $this->Paginator->settings = $this->paginate;
        $this->set('users', $this->Paginator->paginate('User', array('NOT' => array('id' => $this->Auth->user('id'), 'role' => 'root'))));
    }

    public function add()
    {
        if ($this->request->is('post')) {
            if ($this->User->saveAll($this->request->data)) {
                $this->Session->setFlash('Novo usuário adicionado com sucesso.', 'success');
                $this->redirect('index');
            } else {
                $this->Session->setFlash('Ops, algo deu errado. Por favor, verifique os dados e tente novamente.', 'error');
            }
        }
    }

    public function delete($id) {
        if ($id) {
            if ($this->User->delete($id)) {
                $this->Session->setFlash('Usuário removido com sucesso.', 'success');
            } else {
                $this->Session->setFlash('Ops, algo deu errado. Por favor, tente novamente.', 'error');
            }
        }
        $this->redirect('index');
    }

    public function login()
    {
        $this->set('title_for_layout', 'Login');

        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Session->setFlash('Usuário ou Senha incorretos.', 'error');
            }
        }
    }

    public function logout()
    {
        $this->Session->setFlash('Logout efetuado com sucesso.', 'success');
        return $this->redirect($this->Auth->logout());
    }

    public function changepassword()
    {
        if ($this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('Senha alterada com sucesso.', 'success');
                $this->redirect('changepassword');
            } else {
                $this->Session->setFlash('Ocorreu um erro. Por favor, tente novamente.', 'error');
            }
        } else {
            $this->request->data = $this->User->findById($this->Session->read('Auth.User.id'));
        }
    }
}
