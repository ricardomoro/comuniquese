<?php
App::uses('CakeEmail', 'Network/Email');

class MensagensController extends AdminAppController
{
    public $name = 'Mensagens';
    public $uses = array('Mensagem', 'Anexo', 'Contato');

    public $components = array('Paginator');
    public $paginate = array(
        'Mensagem' => array(
            'limit' => 10,
            'order' => array(
                'Mensagem.created' => 'desc',
            ),
        ),
    );

    public $helpers = array('Time');


    public function index()
    {
        $this->Paginator->settings = $this->paginate;
        $this->set('mensagens', $this->Paginator->paginate('Mensagem'));
    }

    public function ver($id)
    {
        if ($id) {
            $this->set('mensagem', $this->Mensagem->findById($id));
        } else {
            $this->redirect('index');
        }
    }

    public function deletar($id = null)
    {
        if ($id) {
            if ($this->Mensagem->delete($id)) {
                $this->Session->setFlash('Mensagem removida com sucesso.', 'success');
                $this->limparUploads();
            } else {
                $this->Session->setFlash('Ops, algo deu errado. Por favor, tente novamente.', 'error');
            }
        }
        $this->redirect('index');
    }
    
    public function reenviar($id = null)
    {
        if ($id) {
            $mensagem = $this->Mensagem->findById($id);

            if ($mensagem['Mensagem']['status'] == Mensagem::STATUS_NAO_ENVIADA) {
                $mail = new CakeEmail();
                $mail->emailFormat('html');
                $mail->template('mensagem');
                $mail->viewVars(array('data' => $mensagem));
                $mail->from(array($mensagem['Mensagem']['email'] => $mensagem['Mensagem']['nome']));
                $mail->replyTo($mensagem['Mensagem']['email']);
                $mail->subject('[Comunique-se!] Informações enviadas pelo site');
                $mail->to($mensagem['Contato']['email']);

                if (isset($mensagem['Anexo']) && !empty($mensagem['Anexo'])) {
                    $anexos = array();
                    foreach ($mensagem['Anexo'] as $key => $anexo) {
                        $anexos[$anexo['nome']] = array('file' => WWW_ROOT.'files/uploads/'.$anexo['arquivo'], 'mimetype' => $anexo['mimetype']);
                    }
                }
                
                if (isset($anexos) && is_array($anexos)) {
                    $mail->attachments($anexos);
                }

                try {
                    $mail->send();
                    $this->Mensagem->id = $mensagem['Mensagem']['id'];
                    $this->Mensagem->saveField('status', Mensagem::STATUS_ENVIADA);
                    $this->Session->setFlash('Mensagem reenviada com sucesso.', 'success');
                } catch (SocketException $e) {
                    $this->Session->setFlash('Ops, algo deu errado. Por favor, tente novamente mais tarde.', 'error');
                }
            }
            $this->redirect('index');
        }
        $this->redirect('index');
    }

    private function limparUploads() {
        $files = scandir('files/uploads');
        $anexos = $this->Anexo->find('all', array('recursive' => '-1'));
        
        $limpou = false;
        
        foreach ($files as $file) {
            if ($file == '.' || $file == '..') {
                continue;
            }
            
            $deletar = true;
            
            foreach ($anexos as $anexo) {
                if ($anexo['Anexo']['arquivo'] == $file) {
                    $deletar = false;
                }
            }
            
            if ($deletar) {
                unlink('files/uploads/'.$file);
            }
        }
    }
}
