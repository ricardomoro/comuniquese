<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="robot" content="noindex, nofollow" />
    <meta name="author" content="Ricardo Moro" />
    <title>Comunique-se! Instala&ccedil;&atilde;o</title>
    <?php
        echo $this->Html->meta('icon');
        echo $this->fetch('meta');
        
        echo $this->Html->css('bootstrap.min');
        echo $this->Html->css('bootstrap-theme.min');
        echo $this->fetch('css');
        
        echo $this->fetch('script');
    ?>
</head>
<body>
    <header class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Instala&ccedil;&atilde;o do Sistema Comunique-se!</h1>
            </div>
        </div>
    </header>
    
    <section class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2><?php echo $title_for_layout; ?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <?php echo $this->Session->flash(); ?>
            </div>
        </div>
    </section>

    <section class="container" role="main">
        <?php echo $this->fetch('content'); ?>
    </section>
    
    <footer class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="well">
                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-md-push-6">
                            <a href="#menu" class="pull-right hidden-sm hidden-xs">Voltar para o topo&nbsp;<span class="glyphicon glyphicon-circle-arrow-up"></span></a>
                            <a href="#menu" class="hidden-md hidden-lg">Voltar para o topo&nbsp;<span class="glyphicon glyphicon-circle-arrow-up"></span></a>
                        </div>
                        <div class="col-xs-12 col-md-6 col-md-pull-6">
                            <p>
                                <p><strong>Projeto Comunique-se!</strong></p>
                                <em>Desenvolvimento</em>
                                <br/>
                                <a href="http://www.ifrs.edu.br/"><strong>Instituto Federal do Rio Grande do Sul</strong></a>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <a href="https://bitbucket.org/ricardomoro/comuniquese/" class="pull-right hidden-sm hidden-xs">C&oacute;digo-fonte sob a licen&ccedil;a GPLv3</a>
                            <a href="https://bitbucket.org/ricardomoro/comuniquese/" class="hidden-md hidden-lg">C&oacute;digo-fonte sob a licen&ccedil;a GPLv3</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <?php echo $this->element('sql_dump'); ?>
</body>
</html>
