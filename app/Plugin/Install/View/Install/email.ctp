<?php echo $this->element('progress', array('porcentagem' => '75')); ?>
<div class="row">
    <div class="col-xs-12">
    <?php
        echo $this->Form->create(false, array(
            'url' => array(
                'plugin' => 'install',
                'controller' => 'install',
                'action' => 'email',
            ),
        ));

        echo $this->Form->input('from', array(
            'label' => 'Email Administrativo',
            'default' => 'you@localhost',
        ));

        echo $this->Form->end('Continuar');
    ?>
    </div>
</div>
