<?php echo $this->element('progress', array('porcentagem' => '25')); ?>
<div class="row">
    <div class="col-xs-12">
    <?php
        echo $this->Form->create(false, array(
            'url' => array(
                'plugin' => 'install',
                'controller' => 'install',
                'action' => 'database',
            ),
        ));
    ?>
        <div class="form-group">
            <label class="col-sm-2 control-label">Banco de Dados</label>
            <div class="col-sm-10">
                <p class="form-control-static">Database/Mysql</p>
            </div>
        </div>
    <?php
        echo $this->Form->input('host', array(
            'label' => 'Endereço do Servidor',
            'default' => 'localhost',
        ));

        echo $this->Form->input('port', array(
            'label' => 'Porta (deixe em branco para usar o padrão)',
        ));

        echo $this->Form->input('login', array(
            'label' => 'Login',
            'default' => 'root',
        ));

        echo $this->Form->input('password', array(
            'label' => 'Senha',
        ));

        echo $this->Form->input('database', array(
            'label' => 'Nome do BD',
            'default' => 'comuniquese',
        ));

        echo $this->Form->end('Continuar');
    ?>
    </div>
</div>
