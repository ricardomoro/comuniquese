<?php
App::uses('Controller', 'Controller');
App::uses('ConnectionManager', 'Model');
App::uses('File', 'Utility');
App::uses('Security', 'Utility');

class InstallController extends InstallAppController
{
    public $name = 'Install';
    public $uses = false;

    public $components = array('Session');

    public $helpers = array(
        'Html',
        'Form' => array(
            'className' => 'Install.BootstrapForm',
        ),
    );

    private function _changeConfiguration($key, $value, $path = null)
    {
        if (!$path) {
            $path = CONFIG . 'core.php';
        }

        $file = new File($path);
        $contents = $file->read();
        $contents = preg_replace('/(?<=Configure::write\(\''.$key.'\', \')([^\' ]+)(?=\'\))/', $value, $contents);

        if ($file->write($contents)) {
            return $value;
        }

        return false;
    }

    private function _generateSecurity() {
        if (copy(CONFIG . 'security.php.install', CONFIG . 'security.php')) {
            $file = new File(CONFIG . 'security.php', true);
            $content = $file->read();
        } else {
            $this->Session->setFlash('Ops, não foi possível achar o arquivo "app/Config/security.php.install". Verifique se o arquivo existe.', 'error');
        }

        $salt = $this->_changeConfiguration('Security.salt', Security::generateAuthKey(), CONFIG . 'security.php');
        $seed = $this->_changeConfiguration('Security.cipherSeed', mt_rand() . mt_rand(), CONFIG . 'security.php');
        
        if (!$salt || !$seed) { 
            $this->Session->setFlash('Ops, não foi possível gerar os códigos de segurança.', 'error');    
            return false;
        }

        return true;
    }

    private function _createDB()
    {
        $file = new File(CONFIG . 'Schema' . DS . 'schema.sql');
        if ($file->exists()) {
            $content = $file->read();
        } else {
            $this->Session->setFlash('Ops, não foi possível achar o arquivo "app/Config/schema.sql". Verifique se o arquivo existe.', 'error');
            return false;
        }

        $db = ConnectionManager::getDataSource('default');
        if ($db->query($content, false)) {
            return true;
        }

        $this->Session->setFlash('Ops, não foi possível criar as tabelas no Banco de Dados. Verifique se o BD está rodando.');
        return false;
    }

    public function index()
    {
        $this->set('title_for_layout', 'Passo 1: Verificação do Ambiente');
    }

    public function database()
    {
        $this->set('title_for_layout', 'Passo 2: Conexão e Criação do Banco de Dados');

        if ($this->request->is('post')) {
            $data = array(
                'Install' => $this->request->data,
            );
            
            $config = array(
                'name' => 'default',
                'datasource' => 'Database/Mysql',
                'persistent' => false,
                'host' => 'localhost',
                'login' => 'root',
                'password' => '',
                'database' => 'comuniquese',
                'schema' => null,
                'prefix' => null,
                'encoding' => 'utf8',
                'port' => null,
            );

            foreach ($data['Install'] as $key => $value) {
                if (isset($data['Install'][$key])) {
                    $config[$key] = $value;
                }
            }

            $result = true;

            if (copy(CONFIG . 'database.php.install', CONFIG . 'database.php')) {
                $file = new File(CONFIG . 'database.php', true);
                $content = $file->read();
            } else {
                $this->Session->setFlash('Ops, não foi possível achar o arquivo "app/Config/database.php.install". Verifique se o arquivo existe.', 'error');
                $result = false;
            }
            
            foreach ($config as $configKey => $configValue) {
                $content = str_replace('{default_' . $configKey . '}', $configValue, $content);
            }

            if (!$file->write($content)) {
               $this->Session->setFlash('Ops, não foi possível escrever no arquivo "database.php".', 'error');
               $result = false;
            }

            try {
                ConnectionManager::create('default', $config);
                $db = ConnectionManager::getDataSource('default');
            } catch (MissingConnectionException $e) {
                $this->Session->setFlash('Ops, ocorreu um erro com o banco de dados: ' . $e->getMessage(), 'error');
                $result = false;
            }

            if (!$db->isConnected()) {
                $this->Session->setFlash('Ops, não foi possível conectar ao banco de dados.', 'error');
                $result = false;
            }
            
            if ($result && $this->_createDB()) {
                if (!$this->_generateSecurity()) {
                    $this->Session->setFlash('Ops, não foi possível gerar as chaves de segurança. Por favor, altere manualmente esses valores no arquivo "/app/Config/core.php" antes de prosseguir, caso contrário seu sistema ficará vulnerável.', 'error');
                }
                $this->redirect(array('action' => 'adminuser'));
            }
        }
    }

    public function adminuser()
    {
        $this->set('title_for_layout', 'Passo 3: Criação do Usuário Administrador');

        if ($this->request->is('post')) {
            $this->loadModel('User');
            $this->request->data['User']['role'] = 'root';

            if ($this->User->save($this->request->data)) {
                $this->redirect(array('action' => 'email'));
            } else {
                $this->Session->setFlash('Ops, ocorreu um erro. Verifique os dados e tente novamente.', 'error');
            }
        }
    }

    public function email()
    {
        $this->set('title_for_layout', 'Passo 4: Configuração do Email');

        if ($this->request->is('post')) {
            $data = array(
                'Install' => $this->request->data,
            );

            $config = array(
                'from' => 'you@localhost',
            );

            foreach ($data['Install'] as $key => $value) {
                if (isset($data['Install'][$key])) {
                    $config[$key] = $value;
                }
            }

            $result = true;

            if (copy(CONFIG . 'email.php.install', CONFIG . 'email.php')) {
                $file = new File(CONFIG . 'email.php', true);
                $content = $file->read();
            } else {
                $this->Session->setFlash('Ops, não foi possível achar o arquivo "app/Config/email.php.install". Verifique se o arquivo existe.', 'error');
                $result = false;
            }
            
            foreach ($config as $configKey => $configValue) {
                $content = str_replace('{default_' . $configKey . '}', $configValue, $content);
            }

            if (!$file->write($content)) {
               $this->Session->setFlash('Ops, não foi possível escrever no arquivo "app/Config/email.php".', 'error');
               $result = false;
            }

            if ($result) {
                $this->redirect(array('action' => 'finish'));
            }
        }
    }

    public function finish()
    {
        $this->set('title_for_layout', 'Passo 5: Finalização');

        if (!$this->_changeConfiguration('Database.installed', 'true', PLUGIN_CONFIG . 'bootstrap.php')) {
            $this->Session->setFlash('Ops, não foi possível modificar o arquivo "app/Plugin/Install/Config/bootstrap.php". Certifique-se que esse arquivo possui permissão de escrita.', 'error');
        }
    }
}
