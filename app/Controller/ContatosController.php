<?php
class ContatosController extends AppController
{
    public $name = 'Contatos';
    public $uses = array('Contato');


    public function index()
    {
        $this->set('contatos', $this->Contato->find('all'));
    }
}
