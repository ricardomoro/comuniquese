<?php
App::uses('CakeEmail', 'Network/Email');

class MensagensController extends AppController
{
    public $name = 'Mensagens';
    public $uses = array('Mensagem', 'Anexo', 'Contato');


    public function index()
    {
        $this->set('contatos', $this->Contato->find('list', array('fields' => array('id', 'nome'))));
    }

    public function enviar()
    {
        $this->loadModel('Mensagem');
        if ($this->request->is('post')) {
            $this->request->data['Mensagem']['status'] = Mensagem::STATUS_NAO_ENVIADA;
            if ($this->Mensagem->saveAll($this->request->data)) {
                $this->Session->setFlash('Informações enviadas com sucesso.', 'success');

                $mail = new CakeEmail();
                $mail->emailFormat('html');
                $mail->template('mensagem');
                $mail->viewVars(array('data' => $this->request->data));
                $mail->from(array($this->request->data['Mensagem']['email'] => $this->request->data['Mensagem']['nome']));
                $mail->replyTo($this->request->data['Mensagem']['email']);
                $mail->subject('[Comunique-se!] Informações enviadas pelo site');
                $contato = $this->Contato->findById($this->request->data['Mensagem']['contato_id']);
                $mail->to($contato['Contato']['email']);

                $msg = $this->Mensagem->read();

                if (isset($msg['Anexo']) && !empty($msg['Anexo'])) {
                    $anexos = array();
                    foreach ($msg['Anexo'] as $key => $anexo) {
                        $anexos[$anexo['nome']] = array('file' => WWW_ROOT.'files/uploads/'.$anexo['arquivo'], 'mimetype' => $anexo['mimetype']);
                    }
                }
                
                if (isset($anexos) && is_array($anexos)) {
                    $mail->attachments($anexos);
                }

                try {
                    $mail->send();
                    $this->Mensagem->saveField('status', Mensagem::STATUS_ENVIADA);
                } catch (SocketException $e) {
                    // Silence is golden.
                }
                
                try {
                    // Envia uma cópia do email para o remetente, por segurança.
                    $mail->to($this->request->data['Mensagem']['email']);
                    $mail->subject('Cópia das informações enviadas pelo site Comunique-se!');
                    $mail->send();
                } catch (SocketException $e) {
                    // Silence is golden.
                }
            } else {
                $this->Session->setFlash('Ops, algo deu errado. Por favor, verifique os dados e tente novamente.', 'error');
            }
        }
        
        $this->redirect('index');
    }

    // Ajax
    public function ajax_add_arquivo()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->layout = 'ajax';
            $this->render('/Elements/forms/anexo');
        } else {
            $this->redirect('/');
        }
    }
}
