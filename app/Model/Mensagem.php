<?php
class Mensagem extends AppModel
{
    const STATUS_NAO_ENVIADA = 0;
    const STATUS_ENVIADA = 1;

    public $name = 'Mensagem';
    public $useTable = 'mensagens';

    public $belongsTo = array(
        'Contato' => array(
            'foreignKey' => 'contato_id',
        ),
    );

    public $hasMany = array(
        'Anexo' => array(
            'foreignKey' => 'mensagem_id',
            'dependent' => true,
        ),
    );

    public $validate = array(
        'oque' => array(
            'regra1' => array(
                'rule' => 'notEmpty',
                'message' => 'Esse campo não pode ser deixado em branco.',
            ),
        ),
        'quem' => array(
            'regra1' => array(
                'rule' => 'notEmpty',
                'message' => 'Esse campo não pode ser deixado em branco.',
            ),
        ),
        'quando' => array(
            'regra1' => array(
                'rule' => 'notEmpty',
                'message' => 'Esse campo não pode ser deixado em branco.',
            ),
        ),
        'onde' => array(
            'regra1' => array(
                'rule' => 'notEmpty',
                'message' => 'Esse campo não pode ser deixado em branco.',
            ),
        ),
        'como' => array(
            'regra1' => array(
                'rule' => 'notEmpty',
                'message' => 'Esse campo não pode ser deixado em branco.',
            ),
        ),
        'porque' => array(
            'regra1' => array(
                'rule' => 'notEmpty',
                'message' => 'Esse campo não pode ser deixado em branco.',
            ),
        ),
        'nome' => array(
            'regra1' => array(
                'rule' => 'notEmpty',
                'message' => 'O Nome Completo não pode ser deixado em branco.',
            ),
            'regra2' => array(
                'rule' => array('custom', '/[a-zA-ZéúíóáÉÚÍÓÁèùìòàÈÙÌÒÀõãñÕÃÑêûîôâÊÛÎÔÂëÿüïöäËYÜÏÖÄ\-\']+$/'),
                'message' => 'O Nome Completo deve conter somente letras, traço(-) e apóstrofo(\').',
            ),
        ),
        'email' => array(
            'regra1' => array(
                'rule' => 'notEmpty',
                'message' => 'O Email não pode ser deixado em branco.',
            ),
            'regra2' => array(
                'rule' => 'email',
                'message' => 'O Email deve ser válido.',
            ),
        ),
        'contato_id' => array(
            'regra1' => array(
                'rule' => 'notEmpty',
                'message' => 'Selecione um destinatário.',
            ),
        ),
    );
}
