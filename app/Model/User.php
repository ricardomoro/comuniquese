<?php
App::uses('AuthComponent', 'Controller/Component');

class User extends AppModel
{
    public $name = 'User';

    public $validate = array(
        'password' => array(
            'regra1' => array(
                'rule' => 'notEmpty',
                'message' => 'Esse campo não pode ser deixado em branco.',
            ),
            'regra2' => array(
                'rule' => array('minLength', 6),
                'message' => 'A senha deve ter no mínimo 6 caracteres.',
            ),
        ),
        'current_password' => array(
            'regra1' => array(
                'rule' => 'notEmpty',
                'message' => 'Esse campo não pode ser deixado em branco.',
            ),
            'regra2' => array(
                'rule' => array('checkCurrentPassword'),
                'message' => 'Senha atual não confere.'
            ),
        ),
        'new_password1' => array(
            'regra1' => array(
                'rule' => 'notEmpty',
                'message' => 'Esse campo não pode ser deixado em branco.',
            ),
            'regra2' => array(
                'rule' => array('minLength', 6),
                'message' => 'A senha deve ter no mínimo 6 caracteres.',
            ),
        ),
        'new_password2' => array(
            'regra1' => array(
                'rule' => 'notEmpty',
                'message' => 'Esse campo não pode ser deixado em branco.',
            ),
            'regra2' => array(
                'rule' => array('checkPasswordsMatch'),
                'message' => 'Campos de não conferem.'
            ),
        ),
    );

    public function checkCurrentPassword($data)
    {
        $this->id = AuthComponent::user('id');
        $password = $this->field('password');
        return (AuthComponent::password($data['current_password']) == $password);
    }

    public function checkPasswordsMatch($data)
    {
        if (isset($this->data[$this->alias]['new_password1']) && isset($this->data[$this->alias]['new_password2'])) {
            return ($this->data[$this->alias]['new_password1'] == $this->data[$this->alias]['new_password2']);
        } else {
            return false;
        }
    }

    public function beforeSave($options = array())
    {
        if (isset($this->data[$this->alias]['new_password1'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['new_password1']);
        }
        if (!isset($this->data[$this->alias]['role'])) {
            $this->data[$this->alias]['role'] = 'admin';
        }
    }
}
