<?php $this->Html->script('add_remove_arquivo', array('inline' => false)); ?>
<?php $this->Html->script('jquery.maskedinput.min', array('inline' => false)); ?>
<?php $this->Html->script('telefone_mask', array('inline' => false)); ?>
<fieldset>
    <legend>Informa&ccedil;&otilde;es b&aacute;sicas</legend>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
        <?php
            echo $this->Form->input('Mensagem.oque', array('type' => 'text', 'before' => '<span class="input-group-addon">O quê?</span>', 'placeholder' => 'O quê?'));
            echo $this->Form->input('Mensagem.quem', array('type' => 'text', 'before' => '<span class="input-group-addon">Quem?</span>', 'placeholder' => 'Quem?'));
            echo $this->Form->input('Mensagem.quando', array('type' => 'text', 'before' => '<span class="input-group-addon">Quando?</span>', 'placeholder' => 'Quando?'));
            echo $this->Form->input('Mensagem.onde', array('type' => 'text', 'before' => '<span class="input-group-addon">Onde?</span>', 'placeholder' => 'Onde?'));
            echo $this->Form->input('Mensagem.como', array('type' => 'text', 'before' => '<span class="input-group-addon">Como?</span>', 'placeholder' => 'Como?'));
            echo $this->Form->input('Mensagem.porque', array('type' => 'text', 'before' => '<span class="input-group-addon">Por quê?</span>', 'placeholder' => 'Por quê?'));
        ?>
        </div>
        <div class="col-xs-12 col-sm-6">
        <?php
            echo $this->Form->input('Mensagem.outras_informacoes', array('type' => 'textarea', 'before' => '<span class="input-group-addon" id="MensagemOutrasInformacoes-addon">Outras informações</span>', 'placeholder' => 'Outras informações'));
        ?>
        </div>
    </div>
</fieldset>
<div class="row">
    <div class="col-xs-12 col-sm-6">
        <fieldset>
            <legend>Identifica&ccedil;&atilde;o e Destino</legend>
        <?php
            echo $this->Form->input('Mensagem.nome', array('type' => 'text', 'before' => '<span class="input-group-addon">Nome completo</span>', 'placeholder' => 'Nome completo'));
            echo $this->Form->input('Mensagem.email', array('type' => 'text', 'before' => '<span class="input-group-addon">Email</span>', 'placeholder' => 'Email'));
            echo $this->Form->input('Mensagem.telefone', array('type' => 'text', 'before' => '<span class="input-group-addon">Telefone</span>', 'id' => 'telefone', 'placeholder' => 'Telefone'));
            echo $this->Form->input('Mensagem.contato_id', array('type' => 'select', 'options' => $contatos, 'before' => '<span class="input-group-addon">Destinatário</span>', 'empty' => 'Selecione...'));
        ?>
        </fieldset>
    </div>
    <div class="col-xs-12 col-sm-6">
        <fieldset id="arquivos">
            <legend>Anexos (fotos ou textos)</legend>
        <?php
            echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span> Adicionar arquivo', '#adicionar_arquivo', array('id' => 'adicionar_arquivo', 'class' => 'btn btn-default btn-sm', 'escape' => false));
        ?>
        <?php
            if ($this->data && isset($this->data['Anexo'])) {
                if (is_array($this->data['Anexo'])) {
                    foreach ($this->data['Anexo'] as $key => $value) {
                        echo $this->element('forms/anexo', array('key' => $key));
                    }
                } else {
                    echo $this->element('forms/anexo');
                }
            }
        ?>
        </fieldset>
    </div>
</div>

<hr/>