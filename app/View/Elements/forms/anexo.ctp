<?php
	if (!isset($key)) {
		$key = explode(' ', microtime());
		$key = explode('.', $key[0]);
		$key = $key[1];
	}

	$remover_link = $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-remove')), '#', array('class' => 'remover_arquivo', 'title' => 'Remover arquivo', 'escape' => false));

	echo $this->Form->input('Anexo.'.$key.'.arquivo', array('type' => 'file', 'before' => $remover_link, 'div' => array('class' => 'input-group file')));
	