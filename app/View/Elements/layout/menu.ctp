<?php $class_active = 'class="active"'; ?>
<nav class="navbar navbar-default" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Alternar Navega&ccedil;&atilde;o</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
            <li <?php echo ($this->params['controller'] == 'mensagens' ? $class_active : ''); ?>><?php echo $this->Html->link('Envie informações', array('controller' => 'mensagens', 'action' => 'index')); ?></li>
            <li <?php echo ($this->params['controller'] == 'pages' && $this->params['pass'][0] == 'instrucoes' ? $class_active : ''); ?>><?php echo $this->Html->link('Instruções', array('controller' => 'pages', 'action' => 'instrucoes')); ?></li>
            <li <?php echo ($this->params['controller'] == 'pages' && $this->params['pass'][0] == 'subsidios' ? $class_active : ''); ?>><?php echo $this->Html->link('Subsídios', array('controller' => 'pages', 'action' => 'subsidios')); ?></li>
            <li class="last<?php echo ($this->params['controller'] == 'contatos' ? ' active' : ''); ?>"><?php echo $this->Html->link('Contatos', array('controller' => 'contatos', 'action' => 'index')); ?></li>
        </ul>
    </div>
</nav>
