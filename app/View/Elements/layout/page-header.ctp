<div id="page-header" class="row">
    <div class="col-xs-12 col-sm-5">
        <h2><?php echo $title; ?></h2>
    </div>
    <div class="col-xs-12 col-sm-7">
        <div class="texto">
        <?php
            if (is_array($text)) {
                foreach ($text as $txt) {
                    echo '<p>' . $txt . '</p>';
                }
            } else {
                echo '<p>' . $text . '</p>';
            }
        ?>
        </div>
    </div>
</div>
